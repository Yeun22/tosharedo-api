<?php

namespace App\Entity;

use ApiPlatform\Core\Annotation\ApiFilter;
use DateTime;
use DateInterval;
use Doctrine\ORM\Mapping as ORM;
use App\Repository\TaskRepository;
use Doctrine\Common\Collections\Collection;
use ApiPlatform\Core\Annotation\ApiResource;
use ApiPlatform\Core\Bridge\Doctrine\Orm\Filter\OrderFilter;
use Doctrine\Common\Collections\ArrayCollection;
use Symfony\Component\Serializer\Annotation\Groups;

/**
 * @ORM\Entity(repositoryClass=TaskRepository::class)
 */
#[
    ApiResource(),
    ApiFilter(OrderFilter::class, properties: ['realisation' => false])
]
class Task
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255)
     */
    #[Groups("task:write")]
    private $title;

    /**
     * @ORM\Column(type="text", nullable=true)
     */
    #[Groups("task:write")]
    private $description;

    /**
     * @ORM\Column(type="boolean")
     */
    private $realisation = false;

    /**
     * @ORM\Column(type="datetime")
     */
    private $createdAt;

    /**
     * @ORM\Column(type="datetime", nullable=true)
     */
    #[Groups("task:write")]
    private $endingDate;

    /**
     * @ORM\Column(type="boolean", nullable=true)
     */
    #[Groups("task:write")]
    private $thisWeek;

    /**
     * @ORM\Column(type="boolean", nullable=true)
     */
    #[Groups("task:write")]
    private $thisDay;

    /**
     * @ORM\Column(type="boolean")
     */
    private $approved = false;

    /**
     * @ORM\ManyToMany(targetEntity=User::class, mappedBy="tasks")
     */
    private $users;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $createdBy;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $assignTo;

    public function __construct()
    {
        $this->users = new ArrayCollection();
        $this->createdAt = new DateTime('now');
        $this->endingDate = $this->createdAt->add(new DateInterval('P10D'));
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getTitle(): ?string
    {
        return $this->title;
    }

    public function setTitle(string $title): self
    {
        $this->title = $title;

        return $this;
    }

    public function getDescription(): ?string
    {
        return $this->description;
    }

    public function setDescription(?string $description): self
    {
        $this->description = $description;

        return $this;
    }

    public function getRealisation(): ?bool
    {
        return $this->realisation;
    }

    public function setRealisation(bool $realisation): self
    {
        $this->realisation = $realisation;

        return $this;
    }

    public function getCreatedAt(): ?\DateTimeInterface
    {
        return $this->createdAt;
    }

    public function setCreatedAt(\DateTimeInterface $createdAt): self
    {
        $this->createdAt = new DateTime('now');

        return $this;
    }

    public function getEndingDate(): ?\DateTimeInterface
    {
        return $this->endingDate;
    }

    public function setEndingDate(\DateTimeInterface $endingDate): self
    {
        $this->endingDate = $endingDate;

        return $this;
    }

    public function getThisWeek(): ?bool
    {
        return $this->thisWeek;
    }

    public function setThisWeek(?bool $thisWeek): self
    {
        $this->thisWeek = $thisWeek;

        return $this;
    }

    public function getThisDay(): ?bool
    {
        return $this->thisDay;
    }

    public function setThisDay(?bool $thisDay): self
    {
        $this->thisDay = $thisDay;

        return $this;
    }

    public function getApproved(): ?bool
    {
        return $this->approved;
    }

    public function setApproved(bool $approved): self
    {
        $this->approved = $approved;

        return $this;
    }

    /**
     * @return Collection|User[]
     */
    public function getUsers(): Collection
    {
        return $this->users;
    }

    public function addUser(User $user): self
    {
        if (!$this->users->contains($user)) {
            $this->users[] = $user;
            $user->addTask($this);
        }

        return $this;
    }

    public function removeUser(User $user): self
    {
        if ($this->users->removeElement($user)) {
            $user->removeTask($this);
        }

        return $this;
    }

    public function getCreatedBy(): ?string
    {
        return $this->createdBy;
    }

    public function setCreatedBy(string $createdBy): self
    {
        $this->createdBy = $createdBy;

        return $this;
    }

    public function getAssignTo(): ?string
    {
        return $this->assignTo;
    }

    public function setAssignTo(string $assignTo): self
    {
        $this->assignTo = $assignTo;

        return $this;
    }
}
